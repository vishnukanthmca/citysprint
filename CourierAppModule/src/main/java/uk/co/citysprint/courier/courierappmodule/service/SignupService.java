package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.model.User;

@Consumes("application/json")
@Produces("application/json")
public interface SignupService {
		
	@POST
	@Path("/")
	public Response createUser(User user) throws Exception;


}
