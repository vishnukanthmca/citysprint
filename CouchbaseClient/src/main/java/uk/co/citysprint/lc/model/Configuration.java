package uk.co.citysprint.lc.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry = 0)
public class Configuration {

	@Id
    private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Configuration(){
		
	}
	
	@Field
	private String dolunaServiceKey;
	
	@Field
	private String brandName;

	@Field
    private String oAuthClientKey;

    @Field
    private String oAuthClientSecret;

	public String getDolunaServiceKey() {
		return dolunaServiceKey;
	}

	public void setDolunaServiceKey(String dolunaServiceKey) {
		this.dolunaServiceKey = dolunaServiceKey;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getoAuthClientKey() {
		return oAuthClientKey;
	}

	public void setoAuthClientKey(String oAuthClientKey) {
		this.oAuthClientKey = oAuthClientKey;
	}

	public String getoAuthClientSecret() {
		return oAuthClientSecret;
	}

	public void setoAuthClientSecret(String oAuthClientSecret) {
		this.oAuthClientSecret = oAuthClientSecret;
	}

	
    
}
