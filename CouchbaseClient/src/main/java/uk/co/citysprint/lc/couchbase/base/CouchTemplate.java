package uk.co.citysprint.lc.couchbase.base;

public interface CouchTemplate<T> {
	
	public void save(T entity) throws Exception;

	public void add(T entity) throws Exception;
	
	public void addLocate(Object entity) throws Exception;
	
	public <T>T get(String id, Class<T> entity) throws Exception;
	
	public boolean exists(String id) throws Exception;
	
	public void remove(T entity) throws Exception;
}
