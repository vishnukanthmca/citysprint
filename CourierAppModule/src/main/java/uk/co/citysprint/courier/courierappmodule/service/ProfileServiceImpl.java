package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import org.apache.cxf.common.util.StringUtils;

import uk.co.citysprint.lc.couchbase.base.CouchTemplate;
import uk.co.citysprint.lc.cxfresponse.MessageHelper;
import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.logging.LCLogger;
import uk.co.citysprint.lc.model.Lookup;
import uk.co.citysprint.lc.model.User;
import uk.co.citysprint.lc.service.AbstractServiceImpl;
import uk.co.citysprint.lc.service.IConstants;

public class ProfileServiceImpl extends AbstractServiceImpl implements ProfileService{

	private static final LCLogger LOGGER = new LCLogger(ProfileServiceImpl.class);
		
	private CouchTemplate<User> dbTemplateImpl;

	@Override
	public Response updateNewMobile(User user) throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId+"updateNewMobile method invoked");
		if(user == null || StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getCallSignNumber()) || StringUtils.isEmpty(user.getUniqueId()) || StringUtils.isEmpty(user.getWorkPhone()) || StringUtils.isEmpty(user.getHomePhone()) || StringUtils.isEmpty(user.getFirstname()) || StringUtils.isEmpty(user.getAddress1()) || StringUtils.isEmpty(user.getTown()) || StringUtils.isEmpty(user.getPostalcode()) || StringUtils.isEmpty(user.getContactTime()) || StringUtils.isEmpty(user.getNearestServiceCenter()) || StringUtils.isEmpty(user.getVechileDetail()) || StringUtils.isEmpty(user.getInsuranceType()) || StringUtils.isEmpty(user.getAgeOfVechile()) || StringUtils.isEmpty(user.getTypeOfOwnership()) || StringUtils.isEmpty(user.getTypeOfDrivingLicense())){
			LOGGER.error(transId+"Error on receiving input");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		
		try{
			this.dbTemplateImpl.save(user);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on updating mobile number on User document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		return getResponseSuccess(user);
	}
	
	@Override
	public Response activateUser(User user) throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId+"activateUser method invoked");
		if(user == null || StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getActivated())){
			LOGGER.error(transId+"Error on receiving input");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		String username = user.getUsername();
		String activation = user.getActivated();
		
		LOGGER.info(transId+"username: "+username);
		LOGGER.info(transId+"activation flag: "+activation);
		
		Lookup lookupDoc = null;
		try{			
			lookupDoc = (Lookup) this.dbTemplateImpl.get(user.getUsername(), Lookup.class);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on retriving User lookup document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		if(lookupDoc == null){
			LOGGER.error(transId + "Error caused on retriving User lookup document:");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.LOOKUP_NOT_FOUND)).build();
		}
		
		try{
			
			user = (User) this.dbTemplateImpl.get(lookupDoc.getLookupDocumentId(), User.class);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on retriving User document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		if(user == null){
			LOGGER.error(transId + "Error caused on retriving User lookup document:");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.USER_RECORD_NOT_FOUND)).build();
		}
		
		if(StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getCallSignNumber()) || StringUtils.isEmpty(user.getUniqueId()) || StringUtils.isEmpty(user.getWorkPhone()) || StringUtils.isEmpty(user.getHomePhone()) || StringUtils.isEmpty(user.getFirstname()) || StringUtils.isEmpty(user.getAddress1()) || StringUtils.isEmpty(user.getTown()) || StringUtils.isEmpty(user.getPostalcode()) || StringUtils.isEmpty(user.getContactTime()) || StringUtils.isEmpty(user.getNearestServiceCenter()) || StringUtils.isEmpty(user.getVechileDetail()) || StringUtils.isEmpty(user.getInsuranceType()) || StringUtils.isEmpty(user.getAgeOfVechile()) || StringUtils.isEmpty(user.getTypeOfOwnership()) || StringUtils.isEmpty(user.getTypeOfDrivingLicense())){
			LOGGER.error(transId+"Error on receiving input");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		
		LOGGER.info(transId+"doc username: "+user.getUsername());
		
		user.setActivated(activation);
		
		try{
			this.dbTemplateImpl.save(user);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on updating mobile number on User document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		return getResponseSuccess(user);
	}
	
	public CouchTemplate<User> getDbTemplateImpl() {
		return dbTemplateImpl;
	}

	public void setDbTemplateImpl(CouchTemplate<User> dbTemplateImpl) {
		this.dbTemplateImpl = dbTemplateImpl;
	}

}
