package uk.co.citysprint.lc.exception;

public class LCException extends Exception
{

    private static final long serialVersionUID = 1997753363232807009L;

		public LCException()
		{
		}

		public LCException(String message)
		{
			super(message);
		}

		public LCException(Throwable cause)
		{
			super(cause);
		}

		public LCException(String message, Throwable cause)
		{
			super(message, cause);
		}

		public LCException(String message, Throwable cause, 
                                           boolean enableSuppression, boolean writableStackTrace)
		{
			super(message, cause, enableSuppression, writableStackTrace);
		}

}