package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.model.User;

@ContextConfiguration(locations="common-context.xml")
public class ProfileServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired private ProfileServiceImpl profile;
	
	@Test
	public void testupdateNewMobileSuccess() throws LCException{
		User user = new User();
		user.setId("um::18885aab-c15b-4392-8147-c3f4e41db1b7");
		user.setUsername("testuser@harmon.com");
		user.setAddress1("High Street");
		user.setAgeOfVechile("1");
		user.setCallSignNumber("1111");
		user.setContactTime("23");
		user.setFirstname("Antony");
		user.setHomePhone("7477869047");
		user.setWorkPhone("7477869047");
		user.setInsuranceType("df");
		user.setNearestServiceCenter("22");
		user.setPassword("0m1Y9TuKTMo=");
		user.setPostalcode("HA1 1JU");
		user.setTown("BA");
		user.setTypeOfDrivingLicense("NA");
		user.setTypeOfOwnership("Vehicle owned");
		user.setUniqueId("34343");
		user.setVechileDetail("djghf");
		Response connect = profile.updateNewMobile(user);
		User data = (User) connect.getEntity();
		Assert.assertNotNull(data);
		Assert.assertEquals("testuser@harmon.com", user.getUsername());
	}
	
	@Test(expected = ClassCastException.class)
	public void testupdateNewMobileErrorOnNull() throws LCException{		
		Response connect = profile.updateNewMobile(null);
		User data = (User) connect.getEntity();		
	}
	
	@Test(expected = ClassCastException.class)
	public void testupdateNewMobileMissedInformation() throws LCException{
		User user = new User();
		user.setId("um::18885aab-c15b-4392-8147-c3f4e41db1b7");
		user.setUsername("testuser@harmon.com");
		user.setAddress1("High Street");
		user.setAgeOfVechile("1");
		user.setCallSignNumber("1111");
		user.setContactTime("23");		
		user.setNearestServiceCenter("22");
		user.setPassword("0m1Y9TuKTMo=");
		user.setPostalcode("HA1 1JU");
		user.setTown("BA");
		user.setTypeOfDrivingLicense("NA");
		user.setTypeOfOwnership("Vehicle owned");
		user.setUniqueId("34343");
		user.setVechileDetail("djghf");
		Response connect = profile.updateNewMobile(user);
		User data = (User) connect.getEntity();
		Assert.assertNotNull(data);
		Assert.assertEquals("testuser@harmon.com", user.getUsername());
	}
	
	@Test(expected = ClassCastException.class)
	public void testupdateNewMobileWithoutId() throws LCException{
		User user = new User();
		user.setUsername("testuser@harmon.com");
		user.setAddress1("High Street");
		user.setAgeOfVechile("1");
		user.setCallSignNumber("1111");
		user.setContactTime("23");
		user.setFirstname("Antony");
		user.setHomePhone("7477869047");
		user.setWorkPhone("7477869047");
		user.setInsuranceType("df");
		user.setNearestServiceCenter("22");
		user.setPassword("0m1Y9TuKTMo=");
		user.setPostalcode("HA1 1JU");
		user.setTown("BA");
		user.setTypeOfDrivingLicense("NA");
		user.setTypeOfOwnership("Vehicle owned");
		user.setUniqueId("34343");
		user.setVechileDetail("djghf");
		Response connect = profile.updateNewMobile(user);
		User data = (User) connect.getEntity();
		Assert.assertNotNull(data);
		Assert.assertEquals("testuser@harmon.com", user.getUsername());
	}
	
	@Test
	public void testactivateUserSuccess() throws LCException{
		User user = new User();
		user.setUsername("testuser@harmon.com");
		user.setActivated("true");
		Response connect = profile.activateUser(user);
		User data = (User) connect.getEntity();
		Assert.assertNotNull(data);
		Assert.assertEquals("testuser@harmon.com", user.getUsername());
	}
	
	@Test(expected = ClassCastException.class)
	public void testactivateUserIncorrectData() throws LCException{
		User user = new User();
		user.setUsername("testuser1@harmon.com");
		user.setActivated("true");
		Response connect = profile.activateUser(user);
		User data = (User) connect.getEntity();		
	}
	
	@Test(expected = ClassCastException.class)
	public void testactivateUserMissingInput() throws LCException{
		User user = new User();
		user.setUsername("testuser1@harmon.com");		
		Response connect = profile.activateUser(user);
		User data = (User) connect.getEntity();		
	}
	
	@Test(expected = ClassCastException.class)
	public void testactivateUserNoInput() throws LCException{
		User user = new User();			
		Response connect = profile.activateUser(user);
		User data = (User) connect.getEntity();		
	}
	
	@Test(expected = ClassCastException.class)
	public void testactivateUserNullInput() throws LCException{		
		Response connect = profile.activateUser(null);
		User data = (User) connect.getEntity();		
	}
}
