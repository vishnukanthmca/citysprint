package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.apache.cxf.common.util.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import uk.co.citysprint.courier.courierappmodule.model.OTPData;
import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.model.User;

@ContextConfiguration(locations="common-context.xml")
public class OTPAccessServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired private OTPAccessServiceImpl otp;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testgetOTPAccessPhoneNumberEmpty() throws ClassCastException, LCException{	
		Response connect = otp.getOTPAccess("");
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		OTPData otpObj = (OTPData) connect.getEntity();	
	}
	
	@Test (expected = ClassCastException.class)
	public void testgetOTPAccessInvalidInput() throws Exception{	
		String phoneNumber = "89545222";
		Response connect = otp.getOTPAccess(phoneNumber);
		OTPData transRef = (OTPData)connect.getEntity();		
		Assert.assertEquals(phoneNumber, transRef.getId());
	}
	
	@Test
	public void testgetOTPAccess() throws Exception{	
		String phoneNumber = "89545222";
		String refNumber = "refnumber";
		persistData(phoneNumber, refNumber);
				
		Response connect = otp.getOTPAccess(phoneNumber);
		OTPData otp = (OTPData)connect.getEntity();
		Assert.assertEquals(phoneNumber, otp.getId());
		Assert.assertFalse(StringUtils.isEmpty(otp.getSmsTransReference()));
		testRemoveDocument(phoneNumber);
	}
	
	@Test
	public void testpersistTransRef() throws Exception{
		String phoneNumber = "895452221";
		String refNumber = "refnumber";
		persistData(phoneNumber, refNumber);
		testRemoveDocument(phoneNumber);
	}
	
	@Test
	public void testpersistTransRefReplaceOld() throws Exception{
		String phoneNumber = "89545222134";
		String refNumber = "refnumber";
		persistData(phoneNumber, refNumber);
		
		phoneNumber = "89545222134";
		refNumber = "new refnumber";
		persistData(phoneNumber, refNumber);
		
		testRemoveDocument(phoneNumber);
	}
	
	@Test
	public void testpersistTransRefMissingPhoneNumber() throws Exception{
		String phoneNumber = "";
		OTPData transRef = new OTPData();
		transRef.setId(phoneNumber);
		transRef.setSmsTransReference("refnumber");
		Response connect = otp.persistTransRef(transRef);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		transRef = (OTPData)connect.getEntity();
		
	}
	
	@Test
	public void testpersistTransRefMissingSMSTransRef() throws Exception{
		String phoneNumber = "7881555";		
		OTPData transRef = new OTPData();
		transRef.setId(phoneNumber);		
		Response connect = otp.persistTransRef(transRef);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		transRef = (OTPData)connect.getEntity();
		
	}
	
	@Test
	public void testValidateUserEmptyInput() throws Exception{
		User user = new User();
		Response connect = otp.validateUser(user);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		user = (User) connect.getEntity();
	}
	
	@Test
	public void testValidateUserWrongInformation() throws Exception{
		User user = new User();
		user.setUsername("wrong");
		user.setUniqueId("111");
		user.setCallSignNumber("111");
		user.setWorkPhone("3333");
		Response connect = otp.validateUser(user);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		user = (User) connect.getEntity();
	}
	
	@Test
	public void testValidateUserWrongUsername() throws Exception{
		User user = new User();
		user.setUsername("wrong");
		user.setUniqueId("34343");
		user.setCallSignNumber("1111");
		user.setWorkPhone("7477869047");
		Response connect = otp.validateUser(user);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		user = (User) connect.getEntity();
	}
	
	@Test
	public void testValidateUserWrongUniqueId() throws Exception{
		User user = new User();
		user.setUsername("testuser@harmon.com");
		user.setUniqueId("3434333");
		user.setCallSignNumber("1111");
		user.setWorkPhone("7477869047");
		Response connect = otp.validateUser(user);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		user = (User) connect.getEntity();
	}
	
	@Test
	public void testValidateUserWrongCallSign() throws Exception{
		User user = new User();
		user.setUsername("testuser@harmon.com");
		user.setUniqueId("34343");
		user.setCallSignNumber("11113");
		user.setWorkPhone("7477869047");
		Response connect = otp.validateUser(user);
		thrown.expect(ClassCastException.class);
		thrown.expectMessage("java.util.ArrayList cannot be cast");
		user = (User) connect.getEntity();
	}
	
	@Test
	public void testValidateUserSuccess() throws Exception{
		User user = new User();
		user.setUsername("testuser@harmon.com");
		user.setUniqueId("34343");
		user.setCallSignNumber("1111");
		user.setWorkPhone("7477869047");
		Response connect = otp.validateUser(user);		
		user = (User) connect.getEntity();
		Assert.assertEquals("testuser@harmon.com", user.getUsername());
	}
	
	private void testRemoveDocument(String phonenumber){
		try{
			OTPData data = new OTPData();
			data.setId(phonenumber);
			otp.remove(data);
		}catch(Exception e){}
	}
	
	private void persistData(String phoneNumber, String refNumber) throws Exception{
		OTPData transRef = new OTPData();
		transRef.setId(phoneNumber);
		transRef.setSmsTransReference(refNumber);
		Response connect = otp.persistTransRef(transRef);
		transRef = (OTPData)connect.getEntity();
		Assert.assertEquals(phoneNumber, transRef.getId());
		Assert.assertEquals(refNumber, transRef.getSmsTransReference());
	}
}
