package uk.co.citysprint.lc.jsonvalidator;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JSONValidator extends AbstractMediator
{

	private static final String JSON_PAYLOAD_PROPERTY = "JSONPayLoad";

	private static final String KEY_SCHEMA_NAME = "jsonSchemaName";

	private static final Log LOGGER = LogFactory.getLog(JSONValidator.class);

	private static final String KEY_VALIDATION_MESSAGE = "validationMessage";

	private static final String KEY_IS_VALID = "isValid";

	private static final String KEY_IS_EXCEPTION = "isException";

	private boolean disabled;

	@Override
	public boolean mediate(MessageContext mc)
	{

		if (disabled || mc.isDoingGET()) return true;

		// mc.setProperty(KEY_IS_EXCEPTION, false);
		// mc.setProperty(KEY_IS_VALID, false);
		// mc.setProperty(KEY_VALIDATION_MESSAGE, "");

		try
		{
			String jsonPayLoad = StringUtils.stripToEmpty((String) mc
			        .getProperty(JSON_PAYLOAD_PROPERTY));

			if (StringUtils.isEmpty(jsonPayLoad))
			{
				LOGGER.info("Property " + JSON_PAYLOAD_PROPERTY
				        + " is empty. Nothing to do here. Returning.");

				return true; // nothing to do.
			}

			String schemaName = StringUtils.stripToEmpty((String) mc
			        .getProperty(KEY_SCHEMA_NAME));

			if (StringUtils.isEmpty(schemaName))
			{
				LOGGER.info("No schema provided. Returning false.");
				throw new Exception("No Schema");
			}

			if (!schemaName.startsWith("/")) schemaName = "/" + schemaName;

			ObjectMapper mapper = new ObjectMapper();

			LOGGER.info("SchemaName " + schemaName);

			InputStream in = getClass().getResourceAsStream(schemaName);

			if (in == null)
			{
				LOGGER.error("Looks like schema " + schemaName
				        + " is not found. I have null input stream. MRS.");
				throw new Exception("Null inputstream");
			}

			BufferedReader reader = new BufferedReader(
			        new InputStreamReader(in));

			JsonNode schemaInFile = mapper.readTree(reader);

			JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
			JsonSchema schema = factory.getJsonSchema(schemaInFile);
			JsonNode rootNode = mapper.readValue(jsonPayLoad, JsonNode.class);
			ProcessingReport report = schema.validate(rootNode);

			boolean blnSuccess = report.isSuccess();

			mc.setProperty(KEY_IS_VALID, blnSuccess);
			mc.setProperty(KEY_VALIDATION_MESSAGE, "");

			LOGGER.info("report.isSuccess() " + blnSuccess);

			if (!blnSuccess)
			{
				LOGGER.error("JSON Schema Validation has failed. Schema name "
				        + schemaName + ". Payload " + jsonPayLoad);
				// mc.setProperty(KEY_VALIDATION_MESSAGE, report.toString());
				mc.setProperty(KEY_VALIDATION_MESSAGE,
				        customMessage(report, mapper));

			}

		}
		catch (Exception ex)
		{

			mc.setProperty(KEY_IS_EXCEPTION, true);
			mc.setProperty(KEY_IS_VALID, false);
			mc.setProperty(KEY_VALIDATION_MESSAGE, ex.getMessage());
			LOGGER.error("EXCEPTION.", ex);
		}

		// Always returns true.
		return true;

	}

	private String customMessage(ProcessingReport report, ObjectMapper mapper)
	        throws JsonProcessingException
	{
		Iterator<ProcessingMessage> itr = report.iterator();

		ProcessingMessage processingMessage;

		JsonNode rootJsonNode, jsonNodePointer;
		String message = "";
		List<Map<String, Map<String, String>>> errorMessages = new ArrayList<Map<String, Map<String, String>>>();

		while (itr.hasNext())
		{

			try
			{
				processingMessage = itr.next();

				message = StringUtils.stripToEmpty(processingMessage
				        .getMessage());

				if (StringUtils.isEmpty(message)) message = "No Message";

				rootJsonNode = processingMessage.asJson();

				jsonNodePointer = rootJsonNode.path("instance").path("pointer");

				Map<String, String> iMap = new TreeMap<String, String>();
				iMap.put("field",
				        jsonNodePointer.toString().replaceAll("\"", ""));
				iMap.put("message", message);

				Map<String, Map<String, String>> oMap = new HashMap<String, Map<String, String>>();

				oMap.put("error", iMap);

				errorMessages.add(oMap);
			}
			catch (Exception e)
			{
				LOGGER.error(e);
			}

		}// while

		return mapper.writeValueAsString(errorMessages);
	}

	public boolean isDisabled()
	{
		return disabled;
	}

	public void setDisabled(boolean disabled)
	{
		this.disabled = disabled;
	}

}