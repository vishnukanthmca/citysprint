package uk.co.citysprint.lc.couchbase.base;

import uk.co.citysprint.lc.model.Config;

public class CouchbaseTemplateImpl<T> implements CouchTemplate<T> {

	private Config config;
	
	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}
	
	public void save(T entity) throws Exception{
		Template.couchbaseTemplate(config).save(entity);
	}	

	public void add(Object entity) throws Exception {
		Template.couchbaseTemplate(config).insert(entity);
	}
	
	public void addLocate(Object entity) throws Exception {
		Template.couchbaseTemplate(config).insert(entity);
	}

	public <T>T get(String id, Class<T> entity) throws Exception {
		return Template.couchbaseTemplate(config).findById(id, entity);
	}
	
	public boolean exists(String id) throws Exception {
		return Template.couchbaseTemplate(config).exists(id);
	}
	
	public void remove(T entity) throws Exception {
		Template.couchbaseTemplate(config).remove(entity);
	}
	
}
