package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import org.apache.cxf.common.util.StringUtils;

import uk.co.citysprint.courier.courierappmodule.model.OTPData;
import uk.co.citysprint.lc.couchbase.base.CouchTemplate;
import uk.co.citysprint.lc.cxfresponse.MessageHelper;
import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.logging.LCLogger;
import uk.co.citysprint.lc.model.Lookup;
import uk.co.citysprint.lc.model.User;
import uk.co.citysprint.lc.service.AbstractServiceImpl;
import uk.co.citysprint.lc.service.IConstants;

public class OTPAccessServiceImpl extends AbstractServiceImpl implements OTPAccessService{
	
	private static final LCLogger LOGGER = new LCLogger(OTPAccessServiceImpl.class);
			
	private CouchTemplate<OTPData> dbTemplateImpl;
		
	public Response getOTPAccess(String phonenumber) throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId + "getOTPAccess method invoked");
				
		String phoneNumber = phonenumber;
				
		if(StringUtils.isEmpty(phonenumber)){
			try{
			phoneNumber = getQueryParams().getFirst("phonenumber");
			}catch(Exception e){}
		}

		LOGGER.debug(transId + "phoneNumber-"+phoneNumber);
		
		if(StringUtils.isEmpty(phoneNumber)){
			LOGGER.error(transId + "Bad Request, phonenumber missing: "+IConstants.BADREQUEST);
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		} 
		OTPData otp = null;
		try {
			otp = this.dbTemplateImpl.get(phoneNumber, OTPData.class);
		} catch (Exception e) {
			LOGGER.error(transId + "Error on retreving transRef details:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		if(otp == null){
			LOGGER.error(transId + "Not found, phonenumber missing: "+IConstants.PHONENUMBER_NOT_EXISTS);
			return Response.status(Response.Status.NOT_FOUND).entity(
					MessageHelper.getOneMessage(IConstants.PHONENUMBER_NOT_EXISTS)).build();
		}
		
		return getResponseSuccess(otp);
				
	}
	
	public Response persistTransRef(OTPData transRef) throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId + "persistTransRef method invoked");
		if(StringUtils.isEmpty(transRef.getId()) || StringUtils.isEmpty(transRef.getSmsTransReference())){
			LOGGER.error(transId + "Error on parameters:");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		try {
			this.dbTemplateImpl.save(transRef);
		} catch (Exception e) {
			LOGGER.error(transId + "Error on saving transRef details:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		return getResponseSuccess(transRef);
	}
	
	@Override
	public Response validateUser(User user) throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId+"validateUser method invoked");
		if(user == null || StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getCallSignNumber()) || StringUtils.isEmpty(user.getUniqueId()) || StringUtils.isEmpty(user.getWorkPhone())){
			LOGGER.error(transId+"Error on receiving input");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		
		String username = user.getUsername();
		String callSign = user.getCallSignNumber();
		String uniqueId = user.getUniqueId();
		String phoneNumber = user.getWorkPhone();
		
		LOGGER.info(transId+"username: "+username);
		LOGGER.info(transId+"callSign: "+callSign);
		LOGGER.info(transId+"uniqueId: "+uniqueId);
		LOGGER.info(transId+"phoneNumber: "+phoneNumber);
		
		Lookup lookupDoc = null;
		try{			
			lookupDoc = (Lookup) this.dbTemplateImpl.get(user.getUsername(), Lookup.class);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on retriving User lookup document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		if(lookupDoc == null){
			LOGGER.error(transId + "Error caused on retriving User lookup document:");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.LOOKUP_NOT_FOUND)).build();
		}
		
		try{
			
			user = (User) this.dbTemplateImpl.get(lookupDoc.getLookupDocumentId(), User.class);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on retriving User document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		if(user == null){
			LOGGER.error(transId + "Error caused on retriving User lookup document:");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.USER_RECORD_NOT_FOUND)).build();
		}
		
		LOGGER.info(transId+"doc username: "+user.getUsername());
		LOGGER.info(transId+"doc callSign: "+user.getCallSignNumber());
		LOGGER.info(transId+"doc uniqueId: "+user.getUniqueId());
		LOGGER.info(transId+"doc phoneNumber: "+user.getWorkPhone());
		if(username.equals(user.getUsername()) && phoneNumber.equals(user.getWorkPhone())){
			if(callSign.equals(user.getCallSignNumber()) && uniqueId.equals(user.getUniqueId())){
				return getResponseSuccess(user);
			}
		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.PHONENUMBER_NOT_EXISTS)).build();
		}
		
		return Response.status(Response.Status.BAD_REQUEST).entity(
				MessageHelper.getOneMessage(IConstants.USER_RECORD_NOT_MATCHING)).build();
	}
	
	public void remove(OTPData data) throws Exception{
		this.dbTemplateImpl.remove(data);
	}
	
	public CouchTemplate<OTPData> getDbTemplateImpl() {
		return dbTemplateImpl;
	}

	public void setDbTemplateImpl(CouchTemplate<OTPData> dbTemplateImpl) {
		this.dbTemplateImpl = dbTemplateImpl;
	}
	
}
