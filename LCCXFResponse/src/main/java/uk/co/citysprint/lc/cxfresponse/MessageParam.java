package uk.co.citysprint.lc.cxfresponse;

public class MessageParam
{

	private String strClass;

	private Object value;

	public String getStrClass()
	{
		return strClass;
	}

	public void setStrClass(String strClass)
	{
		this.strClass = strClass;
	}

	public Object getValue()
	{
		return value;
	}

	public void setValue(Object value)
	{
		this.value = value;
	}

}// class
