package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import uk.co.citysprint.lc.exception.LCException;

@Consumes("application/json")
@Produces("application/json")
public interface ConfigurationService {

	@GET
	@Path("/key")
	public Response getConfigValue() throws LCException;
}
