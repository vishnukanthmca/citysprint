package uk.co.citysprint.lc.service;

public interface IConstants {
	
	public static final String INTERNAL_SERVER_ERROR="LC-EX-COMM-001";
	public static final String IOEXCEPTION = "LC-EX-IO-001";
	public static final String BADREQUEST = "LC-EX-COMM-005";
	public static final String USER_ALREADY_EXISTS = "LC-USER-COMM-001";
	public static final String USER_RECORD_NOT_FOUND = "LC-USER-COMM-002";
	public static final String USER_RECORD_NOT_MATCHING = "LC-USER-COMM-003";
	public static final String PHONENUMBER_NOT_EXISTS = "LC-DATA-COMM-001";
	public static final String LOOKUP_NOT_FOUND = "LC-DATA-COMM-002";
	
}
