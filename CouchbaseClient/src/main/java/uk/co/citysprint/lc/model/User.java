package uk.co.citysprint.lc.model;
import com.couchbase.client.java.repository.annotation.Id;
import com.couchbase.client.java.repository.annotation.Field;

import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;

@Document(expiry = 0)
public class User {

	@Version
    private long version;
	
	@Id
    private String id;
	
	@Field
	private String homePhone;
	
	@Field
	private String workPhone;

	@Field
    private String firstname;

    @Field
    private String lastname;
    
    @Field
    private String username;
    
    @Field
    private String password;
    
    @Field
    private String mobile;
    
	@Field
    private String address1;
    
    @Field
    private String address2;
    
    @Field
    private String town;
    
    @Field
    private String postalcode;
    
    @Field
    private String contactTime;
    
    @Field
    private String nearestServiceCenter;
    
    @Field
    private String vechileDetail;
    
    @Field
    private String insuranceType;
    
    @Field
    private String image;
    
    @Field
    private String homeCountryCode;
        
	@Field
    private String workCountryCode;
    
    @Field
    private String ageOfVechile;
    
    @Field
    private String typeOfDrivingLicense;
    
    @Field
    private String typeOfOwnership;
    
    @Field
    private String callSignNumber;
    
	@Field
    private String uniqueId;
    
    @Field
    private String activated;
		
	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getContactTime() {
		return contactTime;
	}

	public void setContactTime(String contactTime) {
		this.contactTime = contactTime;
	}

	public String getNearestServiceCenter() {
		return nearestServiceCenter;
	}

	public void setNearestServiceCenter(String nearestServiceCenter) {
		this.nearestServiceCenter = nearestServiceCenter;
	}

	public String getVechileDetail() {
		return vechileDetail;
	}

	public void setVechileDetail(String vechileDetail) {
		this.vechileDetail = vechileDetail;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getHomeCountryCode() {
		return homeCountryCode;
	}

	public void setHomeCountryCode(String homeCountryCode) {
		this.homeCountryCode = homeCountryCode;
	}

	public String getWorkCountryCode() {
		return workCountryCode;
	}

	public void setWorkCountryCode(String workCountryCode) {
		this.workCountryCode = workCountryCode;
	}

	public String getAgeOfVechile() {
		return ageOfVechile;
	}

	public void setAgeOfVechile(String ageOfVechile) {
		this.ageOfVechile = ageOfVechile;
	}

	public String getTypeOfDrivingLicense() {
		return typeOfDrivingLicense;
	}

	public void setTypeOfDrivingLicense(String typeOfDrivingLicense) {
		this.typeOfDrivingLicense = typeOfDrivingLicense;
	}

	public String getTypeOfOwnership() {
		return typeOfOwnership;
	}

	public void setTypeOfOwnership(String typeOfOwnership) {
		this.typeOfOwnership = typeOfOwnership;
	}
	

    public String getCallSignNumber() {
		return callSignNumber;
	}

	public void setCallSignNumber(String callSignNumber) {
		this.callSignNumber = callSignNumber;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getActivated() {
		return activated;
	}

	public void setActivated(String activated) {
		this.activated = activated;
	}

	
    public User(String id, String firstname, String lastname, String username, String password) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }
          
    public User(String id){
    	this.id = id;    	
    }
    
    public User(){
    	
    }
    
   
}